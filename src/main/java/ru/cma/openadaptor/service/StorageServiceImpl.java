package ru.cma.openadaptor.service;

import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.cma.openadaptor.dto.FileContent;
import ru.cma.openadaptor.dto.FilePaths;
import ru.cma.openadaptor.service.xml.StaxStreamProcessor;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StorageServiceImpl implements StorageService {
	
	private static final Logger log = LoggerFactory.getLogger(StorageServiceImpl.class);
	private static final int MAX_SAME_NAME_FILES = 50;
	
	@Value("#{systemProperties['openadaptor.mx.path']}")
	private String mxPath;
	
	@Value("#{systemProperties['openadaptor.mt.path']}")
	private String mtPath;
	
	@Override
	public List<FilePaths> load() {
		Path dir = Paths.get(mxPath);
		try {
			
			List<Path> pathsToFiles =
				Files.list(dir)
					.filter(path -> !Files.isDirectory(path))
					.collect(Collectors.toList());
			
			List<FilePaths> result = new ArrayList<>();
			for (Path path : pathsToFiles)
			{
				String ext = FilenameUtils.getExtension(path.getFileName().toString());
				if (!"xml".equals(ext))
					continue;
				
				FilePaths fp = new FilePaths();
				fp.setFileName(FilenameUtils.removeExtension(path.getFileName().toString()));
				fp.setMxPath(dir.toAbsolutePath().toString() + File.separator + fp.getFileName() + ".xml");
				fp.setMtPath(Paths.get(mtPath).toAbsolutePath().toString() + File.separator + fp.getFileName() + ".swf");
				result.add(fp);
			}
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public void moveFile(Path source, Path dest)
	{
		doMoveFile(source, dest, 0);
	}
	
	private void doMoveFile(Path source, Path dest, int incr)
	{
		try
		{
			if (0 == incr)
				FileUtils.moveFile(source.toFile(), dest.toFile());
			else if (MAX_SAME_NAME_FILES == incr)
				throw new RuntimeException("Cannot create more files with the same name, limit  " + MAX_SAME_NAME_FILES + "  is exceeded.");
			else
			{
				String newFileNameDest = dest.toFile().getAbsolutePath() + "." + incr;
				log.info("Trying new filename: {}", newFileNameDest);
				FileUtils.moveFile(source.toFile(), new File(newFileNameDest));
			}
		}
		catch (FileExistsException exists)
		{
			log.warn("File exists in destination, incrementing filename");
			doMoveFile(source, dest, incr + 1);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
	}
	
	@Override
	public void deleteFile(Path file) {
		try {
			Files.delete(file);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public FileContent readXml(Path file) throws IOException, XMLStreamException {
		FileContent content = new FileContent();
	
		try {
			try (StaxStreamProcessor processor = new StaxStreamProcessor(Files.newInputStream(file))) {
				XMLStreamReader reader = processor.getReader();
				FileContent.ParsedInformation parsedInformation = null;
				boolean totalAmountParsed = false;
				String settleDate = null;
				boolean insideDebtorAcc = false;
				
				while (reader.hasNext()) {       // while not end of XML
					int event = reader.next();   // read next event
					if (event == XMLEvent.START_ELEMENT ) {
						if ("TtlIntrBkSttlmAmt".equals(reader.getLocalName())) {
							if (parsedInformation == null)
								parsedInformation = new FileContent.ParsedInformation();
							content.setTotalAmount(new BigDecimal(reader.getElementText()));
							totalAmountParsed = true;
						}
						
						if ("CdtTrfTxInf".equals(reader.getLocalName())) {
							parsedInformation = new FileContent.ParsedInformation();
						}
						if ("IntrBkSttlmAmt".equals(reader.getLocalName()) && parsedInformation != null ) {
							parsedInformation.setAmount(new BigDecimal(reader.getElementText()));
							if (!totalAmountParsed)
								content.setTotalAmount(content.getTotalAmount().add(parsedInformation.getAmount()));
						}
						if ("IntrBkSttlmDt".equals(reader.getLocalName())) {
							settleDate = reader.getElementText();
						}
						// Inside dbtr acct
						if ("DbtrAcct".equals(reader.getLocalName())) {
							insideDebtorAcc = true;
						}
						if ("Id".equals(reader.getLocalName()) && insideDebtorAcc) {
							try {
								parsedInformation.setPayer(reader.getElementText());
							} catch (Exception e) {}
						}
					}
					
					if (event == XMLEvent.END_ELEMENT && "CdtTrfTxInf".equals(reader.getLocalName())) {
						parsedInformation.setDate(settleDate);
						content.getParsedInformations().add(parsedInformation);
					}
					
					if (event == XMLEvent.END_ELEMENT && "DbtrAcct".equals(reader.getLocalName())) {
						insideDebtorAcc = false;
					}
					
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return content;
	}
}
