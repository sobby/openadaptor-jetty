package ru.cma.openadaptor.service;

import ru.cma.openadaptor.dto.FileContent;
import ru.cma.openadaptor.dto.FilePaths;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Stream;

public interface StorageService {
	
	List<FilePaths> load();
	
	void moveFile(Path source, Path dest);
	void deleteFile(Path file);
	
	FileContent readXml(Path file) throws IOException, XMLStreamException;
	
}