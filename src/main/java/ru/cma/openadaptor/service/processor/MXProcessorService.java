package ru.cma.openadaptor.service.processor;

public interface MXProcessorService {
	void approveMX(String fileNameMX, String signedBody) throws Exception;
	void rejectMX(String fileNameMX) throws Exception;
}
