package ru.cma.openadaptor.service.processor;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.cma.openadaptor.service.StorageService;

import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class MXProcessorServiceImpl extends AbstractFileProcessorService implements MXProcessorService {
	
	private StorageService storageService;
	
	private static final Logger log = LoggerFactory.getLogger(MXProcessorServiceImpl.class);
	
	public MXProcessorServiceImpl(StorageService storageService) {
		this.storageService = storageService;
	}
	
	public void approveMX(String fileNameMX, String signedBody) throws Exception {
		log.info("Approving mx {}", fileNameMX);
		final String fullFilenameMX = fileNameMX + ".xml";
		final Path mx = getMX(fullFilenameMX);
		log.info("Writing utf-8 signed body to {}", mx.toFile().getAbsolutePath());
		FileUtils.write(mx.toFile(), signedBody, Charset.forName("UTF-8"));
		// Move signed file
		log.info("Moving file to {}", Paths.get(mxApprovePath, fullFilenameMX).toAbsolutePath().toString());
		storageService.moveFile(mx, Paths.get(mxApprovePath, fullFilenameMX));
	}
	
	public void rejectMX(String fileNameMX) throws Exception {
		final String fullFilenameMX = fileNameMX + ".xml";
		final Path mx = getMX(fullFilenameMX);
		storageService.deleteFile(mx);
	}
	
	private Path getMX(String fileName) {
		return Paths.get(mxPath, fileName);
	}
}
