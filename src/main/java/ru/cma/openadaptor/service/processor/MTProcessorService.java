package ru.cma.openadaptor.service.processor;

public interface MTProcessorService {
	void approveMT(String fileNameMT) throws Exception;
	void rejectMT(String fileNameMT) throws Exception;
}
