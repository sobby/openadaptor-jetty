package ru.cma.openadaptor.service.processor;

import org.springframework.beans.factory.annotation.Value;

public abstract class AbstractFileProcessorService {
	// Путь к отправке подписанных МХ
	@Value("#{systemProperties['openadaptor.send.path']}")
	protected String mxApprovePath;
	// Путь к исходным МХ
	@Value("#{systemProperties['openadaptor.mx.path']}")
	protected String mxPath;
	
	// Путь к исходным МТ
	@Value("#{systemProperties['openadaptor.mt.path']}")
	protected String mtPath;
	// Путь к отмененным МТ
	@Value("#{systemProperties['openadaptor.reject.path']}")
	protected String mtRejectPath;
	// Путь к отправке МТ
	@Value("#{systemProperties['openadaptor.processed.path']}")
	protected String mtApprovePath;
}
