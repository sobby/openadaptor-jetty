package ru.cma.openadaptor.service.processor;

import org.springframework.stereotype.Service;
import ru.cma.openadaptor.service.StorageService;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class MTProcessorServiceImpl extends AbstractFileProcessorService implements MTProcessorService {
	private static final String MT_EXT = ".swf";
	private StorageService storageService;
	
	public MTProcessorServiceImpl(StorageService storageService) {
		this.storageService = storageService;
	}
	
	@Override
	public void approveMT(String fileNameMT) throws Exception {
		final String fullFileNameMT = fileNameMT + MT_EXT;
		Path mtFile = getMT(fullFileNameMT);
		if (Files.exists(mtFile)) {
			storageService.moveFile(mtFile, Paths.get(mtApprovePath, fullFileNameMT));
		}
	}
	
	@Override
	public void rejectMT(String fileNameMT) throws Exception {
		final String fullFileNameMT = fileNameMT + MT_EXT;
		Path mtFile = getMT(fullFileNameMT);
		if (Files.exists(mtFile)) {
			storageService.moveFile(mtFile, Paths.get(mtRejectPath, fullFileNameMT));
		}
	}
	
	private Path getMT(String fileName) {
		return Paths.get(mtPath, fileName);
	}
}
