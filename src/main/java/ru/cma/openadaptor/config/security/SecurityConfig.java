package ru.cma.openadaptor.config.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.jndi.JndiTemplate;
import org.springframework.security.authentication.jaas.AuthorityGranter;
import org.springframework.security.authentication.jaas.DefaultJaasAuthenticationProvider;
import org.springframework.security.authentication.jaas.JaasAuthenticationCallbackHandler;
import org.springframework.security.authentication.jaas.JaasAuthenticationProvider;
import org.springframework.security.authentication.jaas.JaasNameCallbackHandler;
import org.springframework.security.authentication.jaas.JaasPasswordCallbackHandler;
import org.springframework.security.authentication.jaas.memory.InMemoryConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import ru.cma.openadaptor.security.jaas.RoleUserAuthorityGranter;

import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.security.auth.login.AppConfigurationEntry;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


@Configuration
@EnableWebSecurity(debug = false)
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig  extends WebSecurityConfigurerAdapter {
	
	 //USING CLASPATH jass conf
	@Value("classpath:jaas.conf")
	Resource jassLoginConfig;

	@Bean
	public JaasAuthenticationProvider defaultJaasAuthenticationProvider()
	{
		JaasAuthenticationProvider provider = new JaasAuthenticationProvider();
		provider.setCallbackHandlers(new JaasAuthenticationCallbackHandler[]{new JaasNameCallbackHandler(), new JaasPasswordCallbackHandler()} );
		provider.setLoginContextName("stpa");
		provider.setLoginConfig(jassLoginConfig);
		provider.setAuthorityGranters(new AuthorityGranter[]{new RoleUserAuthorityGranter()});
		return provider;
	}

	// OVERRIDING LOGIN MODULE
//	@Bean
//	public DefaultJaasAuthenticationProvider usernameEqualsPassswordProvider() {
//		DefaultJaasAuthenticationProvider provider = new DefaultJaasAuthenticationProvider();
//		Map<String, AppConfigurationEntry[]> map = new HashMap<>();
//
//		AppConfigurationEntry entry =
//			new AppConfigurationEntry("ru.cma.openadaptor.security.jaas.UsernameEqualsPasswordLoginModule",
//				AppConfigurationEntry.LoginModuleControlFlag.REQUIRED, Collections.emptyMap());
//		map.put("SPRINGSECURITY", new AppConfigurationEntry[]{entry});
//		InMemoryConfiguration inMemoryConfiguration = new InMemoryConfiguration(map);
//		provider.setConfiguration(inMemoryConfiguration);
//		provider.setAuthorityGranters(new AuthorityGranter[]{new RoleUserAuthorityGranter()});
//		return provider;
//	}
	
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/resources/*").permitAll();
		http.authorizeRequests().antMatchers("/*").authenticated();
		http.csrf().disable();
		http.authenticationProvider(defaultJaasAuthenticationProvider());
		http.formLogin().loginPage("/login.html").failureUrl("/login-error.html").permitAll();
		http.logout().invalidateHttpSession(true).logoutSuccessUrl("/index.html").logoutUrl("/logout");
	}
	
//	@Bean
//	public JaasAuthenticationProvider defaultJaasAuthenticationProvider()
//	{
//		ClassLoader currentClassloader = Thread.currentThread().getContextClassLoader();
//		Thread.currentThread().setContextClassLoader(getClass().getClassLoader().getParent());
//		JaasAuthenticationProvider provider = null;
//		JndiTemplate jndi = new JndiTemplate();
//		try {
//
//			provider = jndi.lookup("java:comp/env/ejb/jaasProvider", JaasAuthenticationProvider.class);
//			provider.setCallbackHandlers(new JaasAuthenticationCallbackHandler[]{new JaasNameCallbackHandler(), new JaasPasswordCallbackHandler()} );
//			provider.setAuthorityGranters(new AuthorityGranter[]{new RoleUserAuthorityGranter()});
//			Thread.currentThread().setContextClassLoader(currentClassloader);
//			return provider;
//		} catch (NamingException e) {
//			e.printStackTrace();
//			return null;
//		}
//	}
}
