package ru.cma.openadaptor.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class FileContent
{
	private String mtContent;
	private String mxContent;
	private List<ParsedInformation> parsedInformations = new ArrayList<>();
	private BigDecimal totalAmount = BigDecimal.ZERO;
	
	public String getMtContent() {
		return mtContent;
	}
	
	public void setMtContent(String mtContent) {
		this.mtContent = mtContent;
	}
	
	public String getMxContent() {
		return mxContent;
	}
	
	public void setMxContent(String mxContent) {
		this.mxContent = mxContent;
	}
	
	public List<ParsedInformation> getParsedInformations() {
		return parsedInformations;
	}
	
	public void setParsedInformations(List<ParsedInformation> parsedInformations) {
		this.parsedInformations = parsedInformations;
	}
	
	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
	
	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	public static class ParsedInformation {
		private BigDecimal amount;
		private String date;
		private String payer;
		
		
		public BigDecimal getAmount() {
			return amount;
		}
		
		public void setAmount(BigDecimal amount) {
			this.amount = amount;
		}
		
		public String getDate() {
			return date;
		}
		
		public void setDate(String date) {
			this.date = date;
		}
		
		public String getPayer() {
			return payer;
		}
		
		public void setPayer(String payer) {
			this.payer = payer;
		}
	}
}