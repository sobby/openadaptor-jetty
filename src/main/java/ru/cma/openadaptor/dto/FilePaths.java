package ru.cma.openadaptor.dto;


// To display mx mt paths with filename
public class FilePaths
{
	private String mxPath;
	private String mtPath;
	private String fileName;
	
	public String getMxPath() {
		return mxPath;
	}
	
	public void setMxPath(String mxPath) {
		this.mxPath = mxPath;
	}
	
	public String getMtPath() {
		return mtPath;
	}
	
	public void setMtPath(String mtPath) {
		this.mtPath = mtPath;
	}
	
	public String getFileName() {
		return fileName;
	}
	
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
