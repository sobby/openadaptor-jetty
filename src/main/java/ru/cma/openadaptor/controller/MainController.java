package ru.cma.openadaptor.controller;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.cma.openadaptor.dto.FileContent;
import ru.cma.openadaptor.service.StorageService;
import ru.cma.openadaptor.service.processor.MTProcessorService;
import ru.cma.openadaptor.service.processor.MXProcessorService;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;

@Controller
public class MainController {
	private static final Logger log = LoggerFactory.getLogger(MainController.class);
	
	@Autowired
	private StorageService storageService;
	@Autowired
	private MXProcessorService mxProcessorService;
	@Autowired
	private MTProcessorService mtProcessorService;
	
	@Value("#{systemProperties['openadaptor.mx.path']}")
	private String mxPath;
	
	@Value("#{systemProperties['openadaptor.mt.path']}")
	private String mtPath;
	
	@Value("#{systemProperties['openadaptor.form.header'] ?: 'АРМ контроллера платежей НБКР'}")
	private String formHeader;
	
	@Value("#{systemProperties['openadaptor.signer.url'] ?: 'http://localhost:9000/signMX'}")
	private String signerUrl;
	
	@Value("#{systemProperties['openadaptor.reload.timeout.ms'] ?: '15000'}")
	private String reloadTimeout;
	
	@Value("#{systemProperties['openadaptor.disable.sign'] ?: 'false'}")
	private String disableSign;
	
	@GetMapping("/")
	public String index(Model model) {
		model.addAttribute("header", formHeader);
		model.addAttribute("signerUrl", signerUrl);
		model.addAttribute("reloadTimeout", reloadTimeout);
		model.addAttribute("disableSign", disableSign);
		return "index";
	}
	
	@RequestMapping(value = "/availableFiles", method = RequestMethod.GET)
	public String getAvailableFiles(Model model) {
		model.addAttribute("files", storageService.load());
		return "index :: availableFiles";
	}
	
	@RequestMapping(value = "/files", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<FileContent> getFileContent(@RequestParam(value="filename") String filename, @RequestParam boolean escape) {
		
		final String fullFilenameMX = filename + ".xml";
		final String fullFilenameMT = filename + ".swf";
		
		try {
			Path mxFile = getMX(fullFilenameMX);
			byte[] encoded = Files.readAllBytes(mxFile);
			String encodedString = new String(encoded, Charset.defaultCharset());
			
			String encodedMTString = "File not found in mt dir " + fullFilenameMT;
			Path mtFile = getMT(fullFilenameMT);
			if (Files.exists(mtFile)) {
				byte[] encodedMT = Files.readAllBytes(mtFile);
				encodedMTString = new String(encodedMT, Charset.defaultCharset());
			}
			
			FileContent response = storageService.readXml(mxFile);
			response.setMxContent(escape ? StringEscapeUtils.escapeXml11(encodedString) : encodedString);
			response.setMtContent(StringEscapeUtils.escapeXml11(encodedMTString));
			
			ResponseEntity entity = new ResponseEntity(response, HttpStatus.OK);
			return entity;
		}
		catch (IOException | XMLStreamException e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
//	@GetMapping(value = "/approve")
//	@ResponseBody
//	public String approveFile(@RequestParam(value="filename") String filename)
//	{
//		try {
////			mxProcessorService.approveMX(filename);
//			mtProcessorService.approveMT(filename);
//		}
//		// TODO Show error to ui
//		catch (Exception e) {
//			e.printStackTrace();
//		}
//		return "ok";
//	}
	
	@GetMapping(value = "/reject")
	@ResponseBody
	public String rejectFile(@RequestParam(value="filename") String filename)
	{
		try {
			mxProcessorService.rejectMX(filename);
			mtProcessorService.rejectMT(filename);
		}
		// TODO Show error to ui
		catch (Exception e) {
			e.printStackTrace();
		}
		return "ok";
	}
	
	@PostMapping(value = "/approve")
	@ResponseBody
	@SuppressWarnings("unchecked")
	public ResponseEntity<String> approve(@RequestParam String filename, @RequestParam String body) {
		try {
			log.info("Approving MX {}", filename);
			if (disableSign != null && !Boolean.valueOf(disableSign))
			{
				final String decodedBody = java.net.URLDecoder.decode(body, "UTF-8");
				log.info("Decoded \n{}\n into \n {}", body, decodedBody);
				body = decodedBody;
			}
			mxProcessorService.approveMX(filename, body.replaceAll("\\<\\?xml(.+?)\\?\\>", ""));
			log.info("Approving MT");
			mtProcessorService.approveMT(filename);
		}
		// TODO Show error to ui
		catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity("ok", HttpStatus.OK);
	}
	
	private Path getMX(String fileName) {
		return Paths.get(mxPath, fileName);
	}
	
	private Path getMT(String fileName) {
		return Paths.get(mtPath, fileName);
	}
	
}
