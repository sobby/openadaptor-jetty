package ru.cma.openadaptor.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import ru.cma.openadaptor.dto.FileContent;
import ru.cma.openadaptor.service.StorageService;
import ru.cma.openadaptor.service.StorageServiceImpl;

import java.math.BigDecimal;
import java.nio.file.Paths;

public class TestParsing {
	
	@Ignore
	@Test
	public void testParsingPacs008() throws Exception {
		
		StorageService storageService = new StorageServiceImpl();
		FileContent content = storageService.readXml(Paths.get(("src/test/resources/pacs.008.txt")));
		Assert.assertEquals(2, content.getParsedInformations().size());
		Assert.assertEquals("115001XX", content.getParsedInformations().get(0).getPayer());
		Assert.assertEquals("2018-12-20", content.getParsedInformations().get(0).getDate());
		Assert.assertEquals(new BigDecimal("2."), content.getParsedInformations().get(0).getAmount());
		
		Assert.assertEquals("115001XX", content.getParsedInformations().get(1).getPayer());
		Assert.assertEquals("2018-12-20", content.getParsedInformations().get(1).getDate());
		Assert.assertEquals(new BigDecimal("3."), content.getParsedInformations().get(1).getAmount());
		
	}
	
	@Test
	public void testParsingPacs008_3() throws Exception {
		
		StorageService storageService = new StorageServiceImpl();
		FileContent content = storageService.readXml(Paths.get(("src/test/resources/pacs.008.3.txt")));
		Assert.assertEquals(2, content.getParsedInformations().size());
		Assert.assertEquals("4402011241006028", content.getParsedInformations().get(0).getPayer());
		Assert.assertEquals("2018-12-24", content.getParsedInformations().get(0).getDate());
		Assert.assertEquals(new BigDecimal("75000"), content.getParsedInformations().get(0).getAmount());
		
		Assert.assertEquals("4402011103023528", content.getParsedInformations().get(1).getPayer());
		Assert.assertEquals("2018-12-24", content.getParsedInformations().get(1).getDate());
		Assert.assertEquals(new BigDecimal("35952"), content.getParsedInformations().get(1).getAmount());
		
	}
}
