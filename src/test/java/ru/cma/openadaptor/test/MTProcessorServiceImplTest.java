package ru.cma.openadaptor.test;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;
import ru.cma.openadaptor.service.StorageServiceImpl;
import ru.cma.openadaptor.service.processor.MTProcessorService;
import ru.cma.openadaptor.service.processor.MTProcessorServiceImpl;

import java.io.File;

public class MTProcessorServiceImplTest {
	private static final String MT_INITIAL_PATH_FIELD = "mtPath";
	private static final String MT_SEND_PATH_FIELD = "mtApprovePath";
	private static final String MT_REJECT_PATH_FIELD = "mtRejectPath";
	
	@Test
	public void testApproveMT() throws Exception {
		Assert.assertTrue(new File("target/test-classes/initial/test_mt_approve.swf").exists());
		Assert.assertFalse(new File("target/test-classes/mt-processed/test_mt_approve.swf").exists());
		
		MTProcessorService mtProcessorService = new MTProcessorServiceImpl(new StorageServiceImpl());
		ReflectionTestUtils.setField(mtProcessorService, MT_INITIAL_PATH_FIELD, "target/test-classes/initial");
		ReflectionTestUtils.setField(mtProcessorService, MT_SEND_PATH_FIELD, "target/test-classes/mt-processed");
		
		mtProcessorService.approveMT("test_mt_approve");
		Assert.assertFalse(new File("target/test-classes/initial/test_mt_approve.swf").exists());
		Assert.assertTrue(new File("target/test-classes/mt-processed/test_mt_approve.swf").exists());
	}
	
	@Test
	public void testRejectMT() throws Exception {
		
		Assert.assertTrue(new File("target/test-classes/initial/test_mt_reject.swf").exists());
		Assert.assertFalse(new File("target/test-classes/mt-rejected/test_mt_reject.swf").exists());
		
		MTProcessorService mtProcessorService = new MTProcessorServiceImpl(new StorageServiceImpl());
		ReflectionTestUtils.setField(mtProcessorService, MT_INITIAL_PATH_FIELD, "target/test-classes/initial");
		ReflectionTestUtils.setField(mtProcessorService, MT_REJECT_PATH_FIELD, "target/test-classes/mt-rejected");
		
		mtProcessorService.rejectMT("test_mt_reject");
		Assert.assertFalse(new File("target/test-classes/initial/test_mt_reject.swf").exists());
		Assert.assertTrue(new File("target/test-classes/mt-rejected/test_mt_reject.swf").exists());
	}
}
