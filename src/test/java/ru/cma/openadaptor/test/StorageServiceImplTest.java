package ru.cma.openadaptor.test;

import org.junit.Assert;
import org.junit.Test;
import ru.cma.openadaptor.service.StorageService;
import ru.cma.openadaptor.service.StorageServiceImpl;

import java.io.File;
import java.nio.file.Paths;

public class StorageServiceImplTest
{
	@Test
	public void testGivenMultipleSameFilesExists_whenMoveAnother_thenIncrementFilename()
	{
		String sourceStr = "target/test-classes/moving/initial/pacs.008.txt";
		String destStr = "target/test-classes/moving/target/";
		
		StorageService storageService = new StorageServiceImpl();
		storageService.moveFile(Paths.get(sourceStr), Paths.get(destStr, "pacs.008.txt"));
		
		Assert.assertTrue(new File("target/test-classes/moving/target/pacs.008.txt.2").exists());
	}
}
