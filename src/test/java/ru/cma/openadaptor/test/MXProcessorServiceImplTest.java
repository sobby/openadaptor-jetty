package ru.cma.openadaptor.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;
import ru.cma.openadaptor.service.processor.MXProcessorServiceImpl;
import ru.cma.openadaptor.service.StorageServiceImpl;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

public class MXProcessorServiceImplTest {
	private static final String MX_INITIAL_PATH_FIELD = "mxPath";
	private static final String MX_SEND_PATH_FIELD = "mxApprovePath";
	
	@Ignore
	@Test
	public void testApproveMX() throws Exception {
		// assert file content (unsigned data)
		String before = readFile("target/test-classes/initial/test_mx_approve.xml");
		Assert.assertEquals("UNSIGNED", before);
		// approve (sign and move to the same dir)
		MXProcessorServiceImpl mxProcessorServiceImpl = new MXProcessorServiceImpl(new StorageServiceImpl());
		ReflectionTestUtils.setField(mxProcessorServiceImpl, MX_INITIAL_PATH_FIELD, "target/test-classes/initial");
		ReflectionTestUtils.setField(mxProcessorServiceImpl, MX_SEND_PATH_FIELD, "target/test-classes/send");

		mxProcessorServiceImpl.approveMX("test_mx_approve", "SIGNED!");
		// assert signed data
		String after = readFile("target/test-classes/send/test_mx_approve.xml");
		Assert.assertEquals("SIGNED!", after);
	}
	
	@Test
	public void testRejectMX() throws Exception {
		MXProcessorServiceImpl mxProcessorServiceImpl = new MXProcessorServiceImpl(new StorageServiceImpl());
		ReflectionTestUtils.setField(mxProcessorServiceImpl, MX_INITIAL_PATH_FIELD, "target/test-classes/initial");
		ReflectionTestUtils.setField(mxProcessorServiceImpl, MX_SEND_PATH_FIELD, "target/test-classes/send");
		
		mxProcessorServiceImpl.rejectMX("test_mx_reject");
		Assert.assertFalse(new File("target/test-classes/send/test_mx_reject.xml").exists());
		Assert.assertFalse(new File("target/test-classes/initial/test_mx_reject.xml").exists());
	}
	
	private String readFile(String path) throws Exception {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		String data = new String(encoded, Charset.forName("UTF-8"));
		return data;
	}
}
